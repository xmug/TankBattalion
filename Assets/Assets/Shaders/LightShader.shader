Shader "WalkingFat/Toon2/GeometryPointLight"
{
    Properties
    {
        _LightColor ("Light Color", Color) = (1.0, 1.0, 1.0, 1) // 最终输出的灯光颜色，会配合Additive效果
        _Stencil ("Stencil", Int) = 1 // 预设的“Stencil Ref”参考值
    }

    SubShader
    {
        Tags
        { 
            "Queue" = "Geometry+1" // 这里默认是在Geometry上加1，如果要多个灯，就用多个Material，每个的Queue后移一位
            "RenderType"="Opaque"
        }
        LOD 100

        Pass // 第一个Pass只获取背后的轮廓信息
        {
            Blend OneMinusDstColor One // Additive，配合后面的黑色，让效果等于透明（这里不用ColorMask因为它不稳定）

            Stencil
            {
                Ref [_Stencil]     // 设置参考值
                Comp Always        // stencil完全通过
                Pass Replace       // 通过的部分标记为参考值
            }

            ZTest Greater    // 深度测试 大于等于当前最小【深度缓存】中的值时，就会显示。即被物体挡住的部分就会显示 
            ZWrite Off       // 不写入到【深度缓存】

            Cull Front       // 剔除正面，只渲染背面颜色

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 pos : POSITION;
            };
            
            v2f vert (appdata v)
            {
                v2f o;  
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                return fixed4 (0,0,0,1); // 填充黑色，配合Additive等于透明，因为这里不需要显示任何东西。
            }
            ENDCG
        }

        Pass // 第二个Pass用正面当蒙版，并画出Additive当亮色
        {
            Blend OneMinusDstColor One // Additive，这里是要配合最后的灯光颜色，显示出发亮的光效

            Stencil
            {
                Ref [_Stencil]  // 设置参考值
                Comp Equal      // 标记为参考值的部分就显示，就是把上个Pass里的被物体挡住的背面的部分显示出来
            }

            ZWrite Off       // 不写入到【深度缓存】
            Cull Back        // 只显示前面，相当于一个mask把上述的内容抠出来

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 pos : POSITION;
            };

            float4 _LightColor;
            
            v2f vert (appdata v)
            {
                v2f o;  
                o.pos = UnityObjectToClipPos(v.vertex);
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                return _LightColor; // 最终输出灯光颜色
            }
            ENDCG
        }
    }
    Fallback "Unlit/Color"
}