﻿using System;
using TanksMP;

namespace Assets.Scripts.Characters
{
    [Serializable]
    public class PlayerData
    {
        /// <summary>
        /// Maximum health value at game start.
        /// </summary>
        public int maxHealth = 10;
        
        /// <summary>
        /// Delay between shots.
        /// </summary>
        public float fireRate = 0.75f;
        
        /// <summary>
        /// Movement speed in all directions.
        /// </summary>
        public float moveSpeed = 8f;
    }
}