﻿using Photon.Pun;
using TanksMP;
using UnityEngine;

namespace Assets.Scripts.Characters
{
    public class TankController : Player
    {
        // protected override void Shoot(Vector2 direction = default(Vector2))
        // {
        //     if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hitInfo, 1000.0f))
        //     {
        //         if (Time.time > nextFire)
        //         {
        //             //set next shot timestamp
        //             nextFire = Time.time + playerData.fireRate;
        //
        //             //send current client position and turret rotation along to sync the shot position
        //             //also we are sending it as a short array (only x,z - skip y) to save additional bandwidth
        //             short[] pos = new short[] {(short) (shotPos.position.x * 10), (short) (shotPos.position.z * 10)};
        //             //send shot request with origin to server
        //             this.photonView.RPC("CmdShoot", RpcTarget.AllViaServer,
        //                 new object[] {hitInfo.transform.position, pos, turretRotation});
        //         }
        //     }
        // }

        // //called on the server first but forwarded to all clients
        // [PunRPC]
        // protected void CmdShoot(Vector3 targetPos, short[] position, short angle)
        // {
        //     //get current bullet type
        //     int currentBullet = GetView().GetBullet();
        //
        //     //calculate center between shot position sent and current server position (factor 0.6f = 40% client, 60% server)
        //     //this is done to compensate network lag and smoothing it out between both client/server positions
        //     Vector3 shotCenter = Vector3.Lerp(shotPos.position,
        //         new Vector3(position[0] / 10f, shotPos.position.y, position[1] / 10f), 0.6f);
        //     Quaternion syncedRot = turret.rotation = Quaternion.Euler(0, angle, 0);
        //
        //     //spawn bullet using pooling
        //     GameObject obj = PoolManager.Spawn(bullets[currentBullet], shotCenter, syncedRot);
        //     var bullet = obj.GetComponent<Bullet>();
        //     bullet.owner = gameObject;
        //     bullet.targetPos = targetPos;
        //
        //     //check for current ammunition
        //     //let the server decrease special ammunition, if present
        //     if (PhotonNetwork.IsMasterClient && currentBullet != 0)
        //     {
        //         //if ran out of ammo: reset bullet automatically
        //         GetView().DecreaseAmmo(1);
        //     }
        //
        //     //send event to all clients for spawning effects
        //     if (shotFX || shotClip)
        //         RpcOnShot();
        // }
    }
}