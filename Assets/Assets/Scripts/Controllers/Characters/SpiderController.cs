﻿using Assets.Events;
using TanksMP;
using XMUGFramework;

namespace Assets.Scripts.Characters
{
    public class SpiderController : Player
    {
        protected override void Awake()
        {
            base.Awake();
            
            GameCore.Get<EventSystem>().Register<LightBulletEnterEvent>(param =>
            {
                if(gameObject && gameObject.activeInHierarchy && transform == param.targetTransform)
                    TakeDamage(param.bullet);
            });
        }
    }
}