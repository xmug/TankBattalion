﻿using System;
using Assets.Events;
using TanksMP;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts
{
    public class LightBullet : Bullet
    {
        protected override void Awake()
        {
            base.Awake();
            GameCore.Get<EventSystem>().Register<LightBulletEnterEvent>(param =>
            {
                if (hitClip) AudioManager.Play3D(hitClip, transform.position);
            });
        }

        protected override void OnTriggerEnter(Collider col)
        {
            if (col.TryGetComponent<LightBulletCollider>(out LightBulletCollider collider))
                return;
            //cache corresponding gameobject that was hit
            GameObject obj = col.gameObject;
            //try to get a player component out of the collided gameobject
            Player player = obj.GetComponent<Player>();

            //we actually hit a player
            //do further checks
            if (player == null)
            {
                myRigidbody.velocity = Vector3.zero;
            }
        }
    }
}