﻿using System;
using System.Collections.Generic;
using Assets.Events;
using Assets.Scripts.Characters;
using Photon.Pun;
using TanksMP;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts
{
    public class LightBulletCollider : MonoBehaviourPun
    {
        [SerializeField] private LightBullet LightBullet;

        // private List<Player> players = new List<Player>();

        private float ticker = 0.6f;

        private void FixedUpdate()
        {
            transform.position = LightBullet.transform.position;
        }

        private void OnTriggerStay(Collider other)
        {
            if(ticker <= 0)
            {
                ticker = 0.6f;
                //cache corresponding gameobject that was hit
                GameObject obj = other.gameObject;
                //try to get a player component out of the collided gameobject
                Player player = obj.GetComponent<SpiderController>();

                if (LightBullet.owner == null)
                {
                    var tank = transform.GetComponentInParent<TankController>();
                    if (tank)
                    {
                        LightBullet.owner = tank.transform.gameObject;
                    }
                }

                //we actually hit a player
                //do further checks
                if (player != null)
                {
                    GameCore.Get<EventSystem>().Trigger(new LightBulletEnterEvent()
                    {
                        targetTransform = other.transform,
                        bullet = LightBullet
                    });
                }
            }

            ticker -= Time.fixedDeltaTime;
        }

        // private void OnTriggerExit(Collider other)
        // {
        //     //cache corresponding gameobject that was hit
        //     GameObject obj = other.gameObject;
        //     //try to get a player component out of the collided gameobject
        //     Player player = obj.GetComponent<SpiderController>();
        //
        //     if (LightBullet.owner == null)
        //     {
        //         var tank = transform.GetComponentInParent<TankController>();
        //         if (tank)
        //         {
        //             LightBullet.owner = tank.transform.gameObject;
        //         }
        //     }
        //
        //     //we actually hit a player
        //     //do further checks
        //     if (player != null)
        //     {
        //         if (players.Contains(player))
        //         {
        //             players.Remove(player);
        //         }
        //     }
        // }
    }
}