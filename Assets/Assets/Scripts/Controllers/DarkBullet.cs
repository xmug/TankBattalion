﻿using System;
using Assets.Events;
using TanksMP;
using UnityEngine;
using XMUGFramework;

namespace Assets.Scripts
{
    public class DarkBullet : Bullet
    {
        private void Update()
        {
            if(owner!=null)
            {
                if (Physics.SphereCast(transform.position, 0.2f, transform.forward, out var hitInfo))
                {
                    DealWithTriggerEnter(hitInfo.collider);
                }
            }
        }

        protected override void OnTriggerEnter(Collider col)
        {
        }
    }
}