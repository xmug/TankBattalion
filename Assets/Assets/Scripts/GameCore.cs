﻿using Assets.Models;
using UnityEngine;
using XMUGFramework;

namespace Assets
{
    public class GameCore : BaseCore<GameCore>
    {
        protected override void Init()
        {
            Register(new EventSystem());
            Register(new TankModel());
        }
    }
}