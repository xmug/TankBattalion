﻿using TanksMP;
using UnityEngine;
using XMUGFramework;

namespace Assets.Events
{
    public struct LightBulletEnterEvent : IEvent<LightBulletEnterEvent>
    {
        public Bullet bullet;
        public Transform targetTransform;
    }
}